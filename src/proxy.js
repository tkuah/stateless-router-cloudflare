export default {
	async fetch(request, env, ctx) {
		const url = new URL(request.url);

		// Default
		url.hostname = '127.0.0.1';
		url.port = '3000';

		console.log(`New Request is: ${url.toString()}`);

		let redirectHost = await env.GitlabXRedirectCache.get(url.toString())

		if (redirectHost) {
			url.host = redirectHost;

			console.log(`Cached cell, new Request is: ${url.toString()}`);

			return fetch(url.toString(), request);
		}

		// make subrequests with the global `fetch()` function
		let res = await fetch(url.toString(), request);

		let cellRedirect = res.headers.get('X-Gitlab-Cell-Redirect');

		if (cellRedirect) {
			let redirectURL = new URL(cellRedirect);

			await env.GitlabXRedirectCache.put(url.toString(), redirectURL.host, {expirationTtl: 600})

			url.host = redirectURL.host;

			console.log(`Wrong cell, new Request is: ${url.toString()}`);

			return fetch(url.toString(), request);
		}

		return res;
	},
};
